package main

import (
    "time"
)

func main() {
    messages := make(chan string, 1)
    // uncomment for deadlock
    //messages = make(chan string)
    go func() {
        for {
            time.Sleep(2000)
            m := <-messages
            if m == "ping" {
                return // THIS return IS CAUSING THE DEADLOCK
            }
            println(m)
        }
    }()
    messages <- "ping"
    println("sent ping")
    messages <- "pong"
    println("done")
}